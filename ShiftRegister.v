// This is a n-bit shift register
module shiftn (Right, Left, w, Clock, Q);
	
	parameter n=16;
	input [n-1:0] Right;
	input Left, w, Clock;
	output reg [n-1:0]Q;
	integer i;
	always@(posedge Clock)
	if (Left)
		Q <= Right;
	else
	begin
		for(i=0; i<n-1; i=i+1)
			Q[i] <= Q[i+1];
		Q[n-1] <= w;
	end

endmodule
